## OSM PBF 剖析器

### 输入/输出/

1. 输入：OSM PBF文件
2. 输出：剖析后的路网csv文件，写入数据库永久性储存，供其他服务修改

syntax: 
```shell script
<path to json config file>
<path to pbf file>
<path to output csv files>
```

e.g.
```shell script

```

### 开发环境及包依赖

1. 语言：golang 1.14
2. 包依赖：
3. 运维环境
4. 配置方案

### 参考代码

本repo在读取pbf部分参考了开源库 [https://github.com/maguro/pbf] 的实现，特此鸣谢

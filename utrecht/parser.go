package utrecht

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	decoder "wa2-utrecht/decoder"

	"github.com/bitly/go-simplejson"
	"github.com/deckarep/golang-set"
	"github.com/umahmood/haversine"
)

type OSMRaw struct {
	Nodes	map[uint64]*decoder.Node
	Ways    map[uint64]*decoder.Way
	Rels	map[uint64]*decoder.Relation
}

func ParseFile(conf decoder.Options) OSMRaw {
	in, err := os.Open("greater-london.osm.pbf")
	if err != nil {
		log.Fatal(err)
	}
	defer in.Close()

	d, err := decoder.NewDecoder(context.Background(), in, conf)
	if err != nil {
		log.Fatal(err)
	}
	defer d.Close()
	osmRaw := OSMRaw{
		Nodes: make(map[uint64]*decoder.Node),
		Ways:  make(map[uint64]*decoder.Way),
		Rels:  make(map[uint64]*decoder.Relation),
	}
done:
	for {
		v, err := d.Decode()
		switch {
		case err == io.EOF:
			break done
		case err != nil:
			log.Fatal(err)
		default:
			switch v := v.(type) {
			case *decoder.Node:
				osmRaw.Nodes[v.ID] = v
			case *decoder.Way:
				osmRaw.Ways[v.ID] = v
			case *decoder.Relation:
				osmRaw.Rels[v.ID] = v
			default:
				log.Fatalf("unknown type %T\n", v)
			}
		}
	}
	fmt.Printf("Finished reading OSM PBF raw data. \n")
	fmt.Printf("Found Nodes: %d, Ways: %d, Relations: %d\n", len(osmRaw.Nodes), len(osmRaw.Ways), len(osmRaw.Rels))
	return osmRaw
}

func ImportJSONConfig(filename string) *simplejson.Json {
	jsonFile, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	confJSON, err := simplejson.NewJson(byteValue)
	if err != nil {
		log.Fatal(err)
	}
	return confJSON
}

type OSMConfig struct {
	SetRestrict				mapset.Set
	MapForbidden			map[string]interface{}
	MapAccess				map[string]interface{}
	SetHighway 				mapset.Set
	SetOnewayForward 		mapset.Set
	SetOnewayBackward 		mapset.Set
	SetOnewayBoth 			mapset.Set
	SetOnewayClosed			mapset.Set
	MapOnewayMisc 			map[string]interface{}
	MapUsefulNodes			map[string]interface{}
	ArrayUselessTags		[]interface{}
	JSONRoadConfig			*simplejson.Json
}

func OSMParseConfig(filename string) OSMConfig{
	confJSON := ImportJSONConfig(filename)
	osmHighwayJSON := confJSON.Get("osm_highway")
	osmOnewayJSON := confJSON.Get("osm_oneway")
	osmConfig := OSMConfig{
		SetRestrict:       	mapset.NewSetFromSlice(osmHighwayJSON.Get("restrict_tags").MustArray()),
		MapForbidden:      	osmHighwayJSON.Get("forbidden_tags").MustMap(),
		MapAccess:         	osmHighwayJSON.Get("access_tags").MustMap(),
		SetHighway: 		mapset.NewSet(),
		SetOnewayForward:  	mapset.NewSetFromSlice(osmOnewayJSON.Get("forward").MustArray()),
		SetOnewayBackward: 	mapset.NewSetFromSlice(osmOnewayJSON.Get("backward").MustArray()),
		SetOnewayBoth:    	mapset.NewSetFromSlice(osmOnewayJSON.Get("both").MustArray()),
		SetOnewayClosed:	mapset.NewSetFromSlice(osmOnewayJSON.Get("closed").MustArray()),
		MapOnewayMisc:		osmOnewayJSON.Get("misc").MustMap(),
		MapUsefulNodes:     confJSON.Get("osm_node_tags").MustMap(),
		ArrayUselessTags:	confJSON.Get("osm_useless_tags").MustArray(),
		JSONRoadConfig: 	osmHighwayJSON.Get("highway_tags"),
	}
	mapHighway := osmHighwayJSON.Get("highway_tags").MustMap()
	for key, _ := range mapHighway {
		osmConfig.SetHighway.Add(key)
	}
	return osmConfig
}

func FilterWay(input *decoder.Way, osmConfig OSMConfig) bool {
	// TODO: refactor this part to make all the JSON {"blah": ["a", "b"]} query simpler
	for tag, _ := range input.Tags {
		if osmConfig.SetRestrict.Contains(tag) {
			return true
		}
	}
	for tag, value := range input.Tags {
		for forbidden, forbiddenValue := range osmConfig.MapForbidden {
			setForbiddenValue := mapset.NewSetFromSlice(forbiddenValue.([]interface{}))
			if tag == forbidden && setForbiddenValue.Contains(value) {
				return false
			}
		}
	}
	for tag, value := range input.Tags {
		for access, accessValue := range osmConfig.MapAccess {
			setAccessValue := mapset.NewSetFromSlice(accessValue.([]interface{}))
			if tag == access && !setAccessValue.Contains(value) {
				return false
			}
		}
	}
	for tag, value := range input.Tags {
		if tag == "highway" && osmConfig.SetHighway.Contains(value) {
			return true
		}
	}
	return false
}

func FilterWays(input *OSMRaw, osmConfig OSMConfig) {
	for id, way := range (*input).Ways {
		if !FilterWay(way, osmConfig) {
			delete((*input).Ways, id)
		}
	}
	fmt.Printf("Found %d OSM routing ways. \n", len((*input).Ways))
}

type OSMParsed struct {
	RoutingNodes	*mapset.Set
	ModelingNodes	*mapset.Set
	UsefulNodes		*mapset.Set
	Roads    		[]*Road
}

// Note that all road segments are oneways
type Road struct {
	OSMWayId   	uint64
	Name		string
	X			uint64
	Y			uint64
	Highway		string
	NodeIDs 	[]uint64
	Geometry 	[][2]decoder.Degree
	Length		float64
	MaxSpeed	uint8
	Lanes		uint8
	Tags    	map[string]string
}

func FilterNodes(input *OSMRaw, output *OSMParsed, osmConfig OSMConfig) {
	routingNodes := mapset.NewSet()
	modelingNodes := mapset.NewSet()
	usefulNodes := mapset.NewSet()
	mapUseful := osmConfig.MapUsefulNodes

	for _, way := range (*input).Ways {
		for _, NodeID := range way.NodeIDs {
			if modelingNodes.Contains(NodeID) {
				routingNodes.Add(NodeID)
			}
			modelingNodes.Add(NodeID)
		}
		routingNodes.Add(way.NodeIDs[0])
		routingNodes.Add(way.NodeIDs[len(way.NodeIDs) - 1])
	}
	for _, node := range (*input).Nodes {
		tags := node.Tags
		for key, value := range tags {
			for usefulKey, usefulValue := range mapUseful {
				setUsefulValue := mapset.NewSetFromSlice(usefulValue.([]interface{}))
				if key == usefulKey && (setUsefulValue.Contains("*") || setUsefulValue.Contains(value)) {
					usefulNodes.Add(node)
				}
			}
		}
	}
	(*output).RoutingNodes = &routingNodes
	(*output).ModelingNodes = &modelingNodes
	(*output).UsefulNodes = &usefulNodes
}

func IdentifyOneway (tags map[string]string, osmConfig OSMConfig) (bool, bool) {
	// this function do not output error since all cases are caught by both directions
	oneway := tags["oneway"]
	if oneway != "" {
		if osmConfig.SetOnewayBackward.Contains(oneway) {
			return false, true
		}
		if osmConfig.SetOnewayForward.Contains(oneway) {
			return true, false
		}
		if osmConfig.SetOnewayBoth.Contains(oneway) {
			return true, true
		}
		if osmConfig.SetOnewayClosed.Contains(oneway) {
			return false, false
		}
	}
	for key, value := range tags {
		for onewayKey, onewayValue := range osmConfig.MapOnewayMisc {
			setOnewayValue := mapset.NewSetFromSlice(onewayValue.([]interface{}))
			if key == onewayKey && setOnewayValue.Contains(value) {
				return true, false
			}
		}
	}
	return true, true
}

func IdentifyMaxspeedLanes (tags map[string]string, osmConfig OSMConfig) (uint8, uint8) {
	// TODO: identify maxspeed from tags, instead of only from highway tag
	osmHighwayConfig := (*osmConfig.JSONRoadConfig).Get(tags["highway"])
	maxspeed := osmHighwayConfig.Get("maxspeed").MustInt()
	lanes := osmHighwayConfig.Get("lanes").MustInt()
	return uint8(maxspeed), uint8(lanes)
}

func LengthCalculation (nodes []uint64, nodeCoords map[uint64]*decoder.Node) (float64, [][2]decoder.Degree) {
	length := float64(0)
	geometry := make([][2]decoder.Degree, 0)
	for id, node := range nodes{
		geometry = append(geometry, [2]decoder.Degree{nodeCoords[node].Lon, nodeCoords[node].Lat})
		x := haversine.Coord{
			Lat: float64(nodeCoords[node].Lat),
			Lon: float64(nodeCoords[node].Lon),
		}
		y := haversine.Coord{
			Lat: float64(nodeCoords[nodes[id + 1]].Lat),
			Lon: float64(nodeCoords[nodes[id + 1]].Lon),
		}
		_, km := haversine.Distance(x, y)
		length += km * 1000
		if id == len(nodes) - 2 {
			geometry = append(geometry, [2]decoder.Degree{nodeCoords[nodes[id + 1]].Lon, nodeCoords[nodes[id + 1]].Lat})
			break
		}
	}
	return length, geometry
}

func SplitWay(input decoder.Way, routing_nodes *mapset.Set, nodeCoords map[uint64]*decoder.Node, osmConfig OSMConfig) ([]*Road, int, int) {
	ret := make([]*Road, 0)
	forward, backward := IdentifyOneway(input.Tags, osmConfig)
	maxspeed, lanes := IdentifyMaxspeedLanes(input.Tags, osmConfig)
	highway := input.Tags["highway"]
	name := input.Tags["name"]
	for _, key := range osmConfig.ArrayUselessTags {
		delete(input.Tags, key.(string))
	}

	lastRoutingNode := input.NodeIDs[0]
	lastId := 0
	forwardCnt := 0
	backwardCnt := 0
	for id, node := range input.NodeIDs {
		if id > 0 && (*routing_nodes).Contains(node) {
			nodeSegment := input.NodeIDs[lastId:id + 1]
			length, geometry := LengthCalculation(nodeSegment, nodeCoords)
			if forward {
				_ = append(ret, &Road{
					OSMWayId:	input.ID,
					Name:		name,
					X:			lastRoutingNode,
					Y:     		node,
					Highway:  	highway,
					NodeIDs:  	nodeSegment,
					Geometry: 	geometry,
					Length:   	length,
					MaxSpeed: 	maxspeed,
					Lanes:    	lanes,
					Tags:     	input.Tags,
				})
				forwardCnt ++
			}
			if backward {
				_ = append(ret, &Road{
					OSMWayId: 	input.ID,
					Name:     	name,
					X:     		node,
					Y:     		lastRoutingNode,
					Highway:  	highway,
					NodeIDs:  	nodeSegment,
					Geometry: 	geometry,
					Length:   	length,
					MaxSpeed: 	maxspeed,
					Lanes:    	lanes,
					Tags:     	input.Tags,
				})
				backwardCnt ++
			}
			lastRoutingNode = node
			lastId = id
		}
	}
	return ret, forwardCnt, backwardCnt
}

func SplitWays(input *OSMRaw, output *OSMParsed, osmConfig OSMConfig) {
	forwardCnt := 0
	backwardCnt := 0
	for _, way := range input.Ways {
		roads, forwardPlus, backwardPlus := SplitWay(*way, output.RoutingNodes, input.Nodes, osmConfig)
		_ = append((*output).Roads, roads...)
		forwardCnt += forwardPlus
		backwardCnt += backwardPlus
	}
	fmt.Printf("Found %d forward road segments. \n", forwardCnt)
	fmt.Printf("Found %d backward road segments. \n", backwardCnt)
}

func ParseOSMRaw(osmRaw *OSMRaw, osmConfig OSMConfig) *OSMParsed{
	osmParsed := OSMParsed{
		RoutingNodes:  nil,
		ModelingNodes: nil,
		UsefulNodes:   nil,
		Roads:         make([]*Road, 0),
	}
	FilterWays(osmRaw, osmConfig)
	FilterNodes(osmRaw, &osmParsed, osmConfig)
	SplitWays(osmRaw, &osmParsed, osmConfig)
	return &osmParsed
}

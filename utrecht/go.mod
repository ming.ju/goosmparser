module wa2-utrecht

go 1.14

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/deckarep/golang-set v1.7.1
	github.com/dustin/go-humanize v1.0.0
	github.com/golang/protobuf v1.3.2
	github.com/stretchr/testify v1.4.0
	github.com/umahmood/haversine v0.0.0-20151105152445-808ab04add26
)

package decoder

import (
	"fmt"
	"wa2-utrecht/protobuf"

	"github.com/dustin/go-humanize"
	"github.com/golang/protobuf/proto"
)

// Degree is the decimal degree representation of a lon or lat.
type Degree float64

func (d Degree) String() string {
	return fmt.Sprintf("%.7f", d)
}

// toDegree converts a coord into deg, given the offset and granularity of the coord
func toDegree(offset int64, granularity int32, coordinate int64) Degree {
	return 1e-9 * Degree(offset+(int64(granularity)*coordinate))
}

// BoundingBox is simply a bounding box.
type BoundingBox struct {
	Left   Degree
	Right  Degree
	Top    Degree
	Bottom Degree
}

// Contains checks if the bounding box contains the lon lat point.
func (b BoundingBox) Contains(lon Degree, lat Degree) bool {
	return b.Left <= lon && lon <= b.Right && b.Bottom <= lat && lat <= b.Top
}

func (b BoundingBox) String() string {
	return fmt.Sprintf("[%s, %s, %s, %s]",
		humanize.Ftoa(float64(b.Left)), humanize.Ftoa(float64(b.Bottom)),
		humanize.Ftoa(float64(b.Right)), humanize.Ftoa(float64(b.Top)))
}

func parsePrimitiveBlock(buffer []byte) ([]interface{}, error) {
	pb := &protobuf.PrimitiveBlock{}
	if err := proto.Unmarshal(buffer, pb); err != nil {
		return nil, err
	}
	c := newBlockContext(pb)
	elements := make([]interface{}, 0)
	for _, pg := range pb.GetPrimitivegroup() {
		elements = append(elements, c.decodeNodes(pg.GetNodes())...)
		elements = append(elements, c.decodeDenseNodes(pg.GetDense())...)
		elements = append(elements, c.decodeWays(pg.GetWays())...)
		elements = append(elements, c.decodeRelations(pg.GetRelations())...)
	}
	return elements, nil
}

// ElementType is an enumeration of relation types.
type ElementType int

const _ElementType_name = "NODEWAYRELATION"

var _ElementType_index = [...]uint8{0, 4, 7, 15}

func (i ElementType) String() string {
	if i < 0 || i >= ElementType(len(_ElementType_index)-1) {
		return fmt.Sprintf("ElementType(%d)", i)
	}
	return _ElementType_name[_ElementType_index[i]:_ElementType_index[i+1]]
}

const (
	NODE ElementType = iota
	WAY
	RELATION
)

// Node represents a specific point on the earth's surface defined by its <lat, lon>.
// Each node comprises at least an id number and a pair of coordinates.
type Node struct {
	ID   uint64
	Tags map[string]string
	Lat  Degree
	Lon  Degree
}

func (c *blockContext) decodeNodes(nodes []*protobuf.Node) []interface{} {
	elements := make([]interface{}, len(nodes))
	for i, node := range nodes {
		elements[i] = &Node{
			ID:   uint64(node.GetId()),
			Tags: c.decodeTags(node.GetKeys(), node.GetVals()),
			Lat:  toDegree(c.latOffset, c.granularity, node.GetLat()),
			Lon:  toDegree(c.lonOffset, c.granularity, node.GetLon()),
		}
	}
	return elements
}

func (c *blockContext) decodeDenseNodes(nodes *protobuf.DenseNodes) []interface{} {
	ids := nodes.GetId()
	elements := make([]interface{}, len(ids))
	tic := c.newTagsContext(nodes.GetKeysVals())
	lats := nodes.GetLat()
	lons := nodes.GetLon()

	var id, lat, lon int64
	for i := range ids {
		id = ids[i] + id
		lat = lats[i] + lat
		lon = lons[i] + lon

		elements[i] = &Node{
			ID:   uint64(id),
			Tags: tic.decodeTags(),
			Lat:  toDegree(c.latOffset, c.granularity, lat),
			Lon:  toDegree(c.lonOffset, c.granularity, lon),
		}
	}
	return elements
}

type Way struct {
	ID      uint64
	Tags    map[string]string
	NodeIDs []uint64
}

func (c *blockContext) decodeWays(nodes []*protobuf.Way) []interface{} {
	elements := make([]interface{}, len(nodes))
	for i, node := range nodes {
		refs := node.GetRefs()
		nodeIDs := make([]uint64, len(refs))
		var nodeID int64
		for j, delta := range refs {
			nodeID = delta + nodeID
			nodeIDs[j] = uint64(nodeID)
		}
		tags := c.decodeTags(node.GetKeys(), node.GetVals())
		elements[i] = &Way{
			ID:      uint64(node.GetId()),
			Tags:    tags,
			NodeIDs: nodeIDs,
		}
	}
	return elements
}

// Relation is a multi-purpose data structure that documents a relationship
// between two or more data elements (nodes, ways, and/or other relations).
type Relation struct {
	ID      uint64
	Tags    map[string]string
	Members []Member
}

// Member represents an element that
type Member struct {
	ID   uint64
	Type ElementType
	Role string
}

func (c *blockContext) decodeRelations(nodes []*protobuf.Relation) []interface{} {
	elements := make([]interface{}, len(nodes))
	for i, node := range nodes {
		elements[i] = &Relation{
			ID:      uint64(node.GetId()),
			Tags:    c.decodeTags(node.GetKeys(), node.GetVals()),
			Members: c.decodeMembers(node),
		}
	}
	return elements
}

func (c *blockContext) decodeMembers(node *protobuf.Relation) []Member {
	memids := node.GetMemids()
	memtypes := node.GetTypes()
	memroles := node.GetRolesSid()
	members := make([]Member, len(memids))

	var memid int64

	for i := range memids {
		memid = memids[i] + memid
		members[i] = Member{
			ID:   uint64(memid),
			Type: decodeMemberType(memtypes[i]),
			Role: c.strings[memroles[i]],
		}
	}
	return members
}

func (c *blockContext) decodeTags(keyIDs, valIDs []uint32) map[string]string {
	tags := make(map[string]string, len(keyIDs))

	for i, keyID := range keyIDs {
		tags[c.strings[keyID]] = c.strings[valIDs[i]]
	}
	return tags
}

type blockContext struct {
	strings         []string
	granularity     int32
	latOffset       int64
	lonOffset       int64
	dateGranularity int32
}

func newBlockContext(pb *protobuf.PrimitiveBlock) *blockContext {
	return &blockContext{
		strings:         pb.GetStringtable().GetS(),
		granularity:     pb.GetGranularity(),
		latOffset:       pb.GetLatOffset(),
		lonOffset:       pb.GetLonOffset(),
		dateGranularity: pb.GetDateGranularity(),
	}
}

type tagsContext struct {
	strings []string
	i       int
	keyVals []int32
}

func (c *blockContext) newTagsContext(keyVals []int32) *tagsContext {
	tc := &tagsContext{strings: c.strings}

	if len(keyVals) != 0 {
		tc.keyVals = keyVals
	}
	return tc
}

func (tic *tagsContext) decodeTags() map[string]string {
	if tic.keyVals == nil {
		return map[string]string{}
	}
	tags := make(map[string]string)
	i := tic.i

	for tic.keyVals[i] > 0 {
		tags[tic.strings[tic.keyVals[i]]] = tic.strings[tic.keyVals[i+1]]
		i += 2
	}
	tic.i = i + 1
	return tags
}

// decodeMemberType converts protobuf enum Relation_MemberType to a ElementType.
func decodeMemberType(mt protobuf.Relation_MemberType) ElementType {
	switch mt {
	case protobuf.Relation_NODE:
		return NODE
	case protobuf.Relation_WAY:
		return WAY
	case protobuf.Relation_RELATION:
		return RELATION
	default:
		panic("unrecognized member type")
	}
}

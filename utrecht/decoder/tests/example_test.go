package tests

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"os"
	"reflect"
	"runtime"
	"testing"

	decoder2 "wa2-utrecht/decoder"

	parser "wa2-utrecht"
)

const (
	DefaultBufferSize          = 1024 * 1024 // the default buffer size for protobuf un-marshaling
	DefaultInputChannelLength  = 16          // the default channel length of raw blobs
	DefaultOutputChannelLength = 8           // the default channel length of decoded arrays of element

	// DefaultDecodedChannelLength is the default channel length of decoded elements coalesced from output channels
	DefaultDecodedChannelLength = 8000
)

func DefaultNCpu() uint16 {
	return uint16(runtime.GOMAXPROCS(-1))
}

// defaultDecoderConfig provides a default configuration for decoders.
var defaultDecoderConfig = decoder2.Options{
	ProtoBufferSize:      DefaultBufferSize,
	InputChannelLength:   DefaultInputChannelLength,
	OutputChannelLength:  DefaultOutputChannelLength,
	DecodedChannelLength: DefaultDecodedChannelLength,
	NCPU:                 DefaultNCpu(),
}

func TestDecoderStop(t *testing.T) {
	in, err := os.Open("greater-london.osm.pbf")
	if err != nil {
		t.Errorf("Error reading file: %v", err)
	}
	defer in.Close()

	// decode header blob
	decoder, err := decoder2.NewDecoder(context.Background(), in, defaultDecoderConfig)
	if err != nil {
		t.Errorf("Error reading blob header: %v", err)
	}
	assert.Equal(t, reflect.TypeOf(decoder2.OSMHeader{}), reflect.TypeOf(decoder.Header))

	cancel := make(chan bool, 1)
	go func() {
		<-cancel
		decoder.Close()
		decoder.Close()
	}()

	// decode elements
	var nEntries int
	for {
		if nEntries == 1000 {
			cancel <- true
		}
		e, err := decoder.Decode()
		if err != nil {
			if err != io.EOF {
				t.Errorf("Error decoding%v", err)
			} else {
				break
			}
		}
		assert.NotEqual(t, reflect.TypeOf(decoder2.OSMHeader{}), reflect.TypeOf(e))
		nEntries++
	}
	assert.True(t, nEntries < 3200894, "Close did not cancel decoding")
}

func TestExample(t *testing.T) {
	osmRaw := parser.ParseFile(defaultDecoderConfig)
	assert.Equal(t, len(osmRaw.Nodes), 2729006, "Number of nodes not as expected!")
	assert.Equal(t, len(osmRaw.Ways), 459055, "Number of ways not as expected!")
	assert.Equal(t, len(osmRaw.Rels), 12833, "Number of relations not as expected!")

	osmConfig := parser.OSMParseConfig("../../config/default.json")
	osmParsed := parser.ParseOSMRaw(&osmRaw, osmConfig)

	assert.Equal(t, len(osmRaw.Ways), 122533, "Number of road ways not as expected!")
	assert.Equal(t, (*osmParsed.RoutingNodes).Cardinality(), 185655, "Number of routing nodes not as expected!")
	assert.Equal(t, (*osmParsed.ModelingNodes).Cardinality(), 476138, "Number of modeling nodes not as expected!")
	fmt.Printf("Found %d routing nodes. \n", (*osmParsed.RoutingNodes).Cardinality())
	fmt.Printf("Found %d modelling nodes. \n", (*osmParsed.ModelingNodes).Cardinality())
	fmt.Printf("Found %d useful nodes. \n", (*osmParsed.UsefulNodes).Cardinality())
}

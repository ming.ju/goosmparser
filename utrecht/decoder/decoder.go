package decoder

import (
	"bytes"
	"compress/zlib"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"reflect"
	"time"
	"wa2-utrecht/protobuf"

	"github.com/golang/protobuf/proto"
)

type encoded struct {
	header *protobuf.BlobHeader
	blob   *protobuf.Blob
	err    error
}

type decoded struct {
	elements []interface{}
	err      error
}

type pair struct {
	element interface{}
	err     error
}

// Decoder reads and decodes OpenStreetMap PBF data from an input stream.
type Decoder struct {
	Header OSMHeader
	pairs  chan pair
	cancel context.CancelFunc
}

// Options provides optional configuration parameters for Decoder construction.
type Options struct {
	ProtoBufferSize      int    // buffer size for protobuf un-marshaling
	InputChannelLength   int    // channel length of raw blobs
	OutputChannelLength  int    // channel length of decoded arrays of element
	DecodedChannelLength int    // channel length of decoded elements coalesced from output channels
	NCPU                 uint16 // the number of CPUs to use for background processing
}

// NewDecoder returns a new decoder, configured with cfg, that reads from
// reader.  The decoder is initialized with the OSM header.
func NewDecoder(ctx context.Context, reader io.Reader, conf Options) (*Decoder, error) {
	d := &Decoder{}
	ctx, d.cancel = context.WithCancel(ctx)

	r := newBlobReader(reader)
	buf := bytes.NewBuffer(make([]byte, 0, conf.ProtoBufferSize))
	h, err := r.readBlobHeader(buf)
	if err != nil {
		return nil, err
	}
	b, err := r.readBlob(buf, h)
	if err != nil {
		return nil, err
	}
	e, err := elements(h, b, bytes.NewBuffer(make([]byte, 0, 1024)))
	if err != nil {
		return nil, err
	}
	if e[0].(*OSMHeader) == nil {
		err = fmt.Errorf("expected header data but got %v", reflect.TypeOf(e[0]))
		return nil, err
	}
	d.Header = *e[0].(*OSMHeader)

	// create decoding pipelines
	var outputs []chan decoded
	for _, input := range read(ctx, r, conf) {
		outputs = append(outputs, decode(input, conf))
	}

	d.pairs = coalesce(conf, outputs...)
	return d, nil
}

// Decode reads the next OSM object and returns either a pointer to Node, Way or Relation struct
// representing the underlying OpenStreetMap PBF data, or error encountered.
// The end of the input stream is reported by an io.EOF error.
func (d *Decoder) Decode() (interface{}, error) {
	decoded, more := <-d.pairs
	if !more {
		return nil, io.EOF
	}
	return decoded.element, decoded.err
}

// Close will cancel the background decoding pipeline.
func (d *Decoder) Close() {
	d.cancel()
}

// read obtains OSM blobs and sends them down, in a round-robin manner, a list of channels to be decoded.
func read(ctx context.Context, b blobReader, conf Options) (inputs []chan encoded) {
	n := conf.NCPU
	for i := uint16(0); i < n; i++ {
		inputs = append(inputs, make(chan encoded, conf.InputChannelLength))
	}
	go func() {
		defer func() {
			for _, input := range inputs {
				close(input)
			}
		}()
		buffer := bytes.NewBuffer(make([]byte, 0, conf.ProtoBufferSize))
		var i uint16
		for {
			input := inputs[i]
			i = (i + 1) % n
			h, err := b.readBlobHeader(buffer)
			if err == io.EOF {
				return
			} else if err != nil {
				input <- encoded{err: err}
				return
			}
			b, err := b.readBlob(buffer, h)
			if err != nil {
				input <- encoded{err: err}
				return
			}
			select {
			case <-ctx.Done():
				return
			case input <- encoded{header: h, blob: b}:
			}
		}
	}()
	return inputs
}

// decode decodes blob/header pairs into an array of OSM elements.  These arrays are
// placed onto an output channel where they will be coalesced into their correct order.
func decode(input <-chan encoded, conf Options) (output chan decoded) {
	output = make(chan decoded, conf.OutputChannelLength)
	buf := bytes.NewBuffer(make([]byte, 0, conf.ProtoBufferSize))
	go func() {
		defer close(output)
		for {
			raw, more := <-input
			if !more {
				return
			}
			if raw.err != nil {
				output <- decoded{nil, raw.err}
				return
			}
			elements, err := elements(raw.header, raw.blob, buf)
			output <- decoded{elements, err}
		}
	}()
	return
}

// coalesce merges the list of channels in a round-robin manner and sends the
// elements in pairs down a channel of pairs.
func coalesce(conf Options, outputs ...chan decoded) (pairs chan pair) {
	pairs = make(chan pair, conf.DecodedChannelLength)
	go func() {
		defer close(pairs)
		n := len(outputs)
		var i int
		for {
			output := outputs[i]
			i = (i + 1) % n
			decoded, more := <-output
			if !more {
				// Since the channels are inspected round-robin, when one channel
				// is done, all subsequent channels are done.
				return
			}
			if decoded.err != nil {
				pairs <- pair{nil, decoded.err}
				return
			}
			for _, e := range decoded.elements {
				pairs <- pair{e, nil}
			}
		}
	}()
	return pairs
}

// elements unmarshals an array of OSM elements from an array of protobuf encoded bytes.
// The bytes could possibly be compressed; zlibBuf is used to facilitate decompression.
func elements(header *protobuf.BlobHeader, blob *protobuf.Blob, zlibBuf *bytes.Buffer) ([]interface{}, error) {
	var buf []byte

	switch {
	case blob.Raw != nil:
		buf = blob.GetRaw()

	case blob.ZlibData != nil:
		r, err := zlib.NewReader(bytes.NewReader(blob.GetZlibData()))
		if err != nil {
			return nil, err
		}
		zlibBuf.Reset()
		rawBufferSize := int(blob.GetRawSize() + bytes.MinRead)
		if rawBufferSize > zlibBuf.Cap() {
			zlibBuf.Grow(rawBufferSize)
		}
		_, err = zlibBuf.ReadFrom(r)
		if err != nil {
			return nil, err
		}
		if zlibBuf.Len() != int(blob.GetRawSize()) {
			err = fmt.Errorf("raw blob data size %d but expected %d", zlibBuf.Len(), blob.GetRawSize())
			return nil, err
		}
		buf = zlibBuf.Bytes()
	default:
		return nil, errors.New("unknown blob data type")
	}
	ht := *header.Type

	switch ht {
	case "OSMHeader":
		{
			h, err := parseOSMHeader(buf)
			if err != nil {
				return nil, err
			}
			return []interface{}{h}, nil
		}
	case "OSMData":
		return parsePrimitiveBlock(buf)
	default:
		return nil, fmt.Errorf("unknown header type %s", ht)
	}
}

// OSMHeader is the contents of the OpenStreetMap PBF data file.
type OSMHeader struct {
	BoundingBox                      BoundingBox
	RequiredFeatures                 []string
	OptionalFeatures                 []string
	WritingProgram                   string
	Source                           string
	OsmosisReplicationTimestamp      time.Time
	OsmosisReplicationSequenceNumber int64
	OsmosisReplicationBaseURL        string
}

// parseOSMHeader unmarshals the OSM header from an array of protobuf encoded bytes.
func parseOSMHeader(buffer []byte) (*OSMHeader, error) {
	hb := &protobuf.HeaderBlock{}
	if err := proto.Unmarshal(buffer, hb); err != nil {
		return nil, err
	}
	header := &OSMHeader{
		RequiredFeatures:                 hb.GetRequiredFeatures(),
		OptionalFeatures:                 hb.GetOptionalFeatures(),
		WritingProgram:                   hb.GetWritingprogram(),
		Source:                           hb.GetSource(),
		OsmosisReplicationBaseURL:        hb.GetOsmosisReplicationBaseUrl(),
		OsmosisReplicationSequenceNumber: hb.GetOsmosisReplicationSequenceNumber(),
	}
	if hb.Bbox != nil {
		header.BoundingBox = BoundingBox{
			Left:   toDegree(0, 1, hb.Bbox.GetLeft()),
			Right:  toDegree(0, 1, hb.Bbox.GetRight()),
			Top:    toDegree(0, 1, hb.Bbox.GetTop()),
			Bottom: toDegree(0, 1, hb.Bbox.GetBottom()),
		}
	}
	if hb.OsmosisReplicationTimestamp != nil {
		header.OsmosisReplicationTimestamp = time.Unix(*hb.OsmosisReplicationTimestamp, 0)
	}
	return header, nil
}

type blobReader struct {
	r io.Reader
}

func newBlobReader(r io.Reader) blobReader {
	return blobReader{r: r}
}

// readBlobHeader unmarshals a header from an array of protobuf encoded bytes.
// The header is used when decoding blobs into OSM elements.
func (b blobReader) readBlobHeader(buffer *bytes.Buffer) (header *protobuf.BlobHeader, err error) {
	var size uint32
	err = binary.Read(b.r, binary.BigEndian, &size)
	if err != nil {
		return nil, err
	}
	buffer.Reset()
	if _, err := io.CopyN(buffer, b.r, int64(size)); err != nil {
		return nil, err
	}
	header = &protobuf.BlobHeader{}
	if err := proto.UnmarshalMerge(buffer.Bytes(), header); err != nil {
		return nil, err
	}
	return header, nil
}

// readBlob unmarshals a blob from an array of protobuf encoded bytes.  The
// blob still needs to be decoded into OSM elements using decode().
func (b blobReader) readBlob(buffer *bytes.Buffer, header *protobuf.BlobHeader) (*protobuf.Blob, error) {
	size := header.GetDatasize()
	buffer.Reset()
	if _, err := io.CopyN(buffer, b.r, int64(size)); err != nil {
		return nil, err
	}
	blob := &protobuf.Blob{}
	if err := proto.UnmarshalMerge(buffer.Bytes(), blob); err != nil {
		return nil, err
	}
	return blob, nil
}

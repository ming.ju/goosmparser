### OSM 道路网络：XML 格式的原始数据

请注意OSM XML中储存着大量的其他数据，对于我们寻路过程中不太关心的数据，限于篇幅就不再赘述了。

#### 节点（node）

```xml
<node id="25496583" lat="51.5173639" lon="-0.140043">
    <tag k="highway" v="traffic_signals"/>
</node>
```

1. 在OSM XML格式中以`<id, lat, lon>`格式保存
2. id：出于扩展性原因，OSM一律以64位整数储存id（int64，下同）
3. 经纬度：
    1. 按照 WGS84 投影，经线上每一个纬度的长度大约是相等的，大约110km；纬线上每一个经度随纬度不同，从南北极的0，到赤道的大约110km
    2. 因此 OSM 内部储存的经纬度精确到小数点后七位，也就是1cm，低于这个精度的经纬度转换会引起误差

#### 路（way）

```xml
<way id="5090250" visible="true">
  <nd ref="822403"/>
  <nd ref="21533912"/>
  <nd ref="821601"/>
  <nd ref="21533910"/>
  <nd ref="135791608"/>
  <nd ref="333725784"/>
  <nd ref="333725781"/>
  <nd ref="333725774"/>
  <nd ref="333725776"/>
  <nd ref="823771"/>
  <tag k="highway" v="residential"/>
  <tag k="name" v="Clipstone Street"/>
  <tag k="oneway" v="yes"/>
</way>
```

1. 参考（ref）：一个包含若干个节点id的数组，刻画了道路的几何形状
    1. 我们可以通过链接节点id，根据半正矢公式（Haversine）计算出来道路的长度
    2. OSM的路并不等同于图（graph）中的边（有向边 arc，或者无向边 edge），需要我们手动裁切
2. 标记（tags）：一个字典，我们非常关心其中的一些内容
    1. 公路（highway）：标志着道路的层级，从高速公路到乡间小路
    2. 单行道（oneway）：标志着单行道，在我们道路网络剖析的最后，所有的道路都是要标记成单行道的（笑）
    3. 限速（maxspeed）：一般来说公路本身标志着限速，但很多时候道路会有特殊的限速标记
3. __TODO:__ 关系（relation）：在这里我们关心的内容比较有限，我们只关心禁止左转禁止右转类似的标记
    1. 必须转弯（mandatory restriction）
    2. 禁止转弯（prohibitory restriction）

#### GeoJSON

我们会使用 GeoJSON 的 **折线**（polyline）功能进行路网可视化。

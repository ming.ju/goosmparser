### OSM PBF 格式浅析

XML或者JSON可读性强的反面就是他们占用的磁盘空间太大了。Google开发的Protobuf格式很好的解决了这个问题。

#### 文件块（fileblock）

PBF 文件格式提供到文件块粒度的随机访问能力，每个文件块都可以单独被剖析，包含一系列被编码的原始群（PrimitiveGroups）。每个原始群中包含大约8,000个OSM实体。目前的PBF序列化器（Osmosis）保存了OSM实体的顺序和标记（tag）。

#### 文件头（OSMHeader）

每个PBF文件包含一个文件头，和若干个文件块。

#### 实际数据（Raw Data）

##### 节点（Nodes）和密集节点（Dense Nodes）


##### 道路（Way）和关系（Relation）

